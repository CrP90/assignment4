/*
 * File: Hangman.java
 * ------------------
 * This program will eventually play the Hangman game from
 * Assignment #4.
 */

import acm.graphics.*;
import acm.program.*;
import acm.util.*;

import java.awt.*;

public class Hangman extends ConsoleProgram {
	
	/** This method initializes the canvas and adds it to the window.
	 * Since this is a console program, the console is already installed 
	 * and will therefore show up in the left column. Canvas it will occupy the 
	 * second column, which means that the console and graphics components of the 
	 * window will each get half the screen area
	 */
	public void init() {
		canvas = new HangmanCanvas();  // create a instance of HangmanLexicon class
		add(canvas);
	}
	
	/** Runs the Hangman game */
    public void run() {
		lexicon = new HangmanLexicon(); // create a instance of HangmanLexicon class
		while (playAnotherGame.startsWith("y") || playAnotherGame.startsWith("Y")){
			setGame();
			playGame();
		}
	}
    /** This method displays a welcome message and chooses a secret word */
    private void setGame() {
    	canvas.reset();
    	println ("Welcome to Hangman!");
    	newWord = secretWord();
    	guessesLeft = 8;
    	System.out.println(newWord);
    }
    /** This method will play the game until guesses remaining */
    private void playGame() {
    	String result = dashedLine(newWord); //initialy the result is a dashed line
    	/* Storing a temporary value of result. Whenever the user guess a letter
    	 * temp will be updated with the new result. After each guess the result will
    	 * be compared to temp, if they`re the same means the user failed to guess a valid letter.
    	 */
    	String temp = result; 
    	while (guessesLeft > 0){
    		println("The word now look like this: " + result);
    		if (guessesLeft > 1) {
    			println ("You have " + guessesLeft + " guesses left.");
    		} else println("You have only one guess!");
        	String guess = readLine("Your guess: ");
        	while (!isValid(guess)) {
        		println ("Your guess is illegal. Please select a single letter.");
        		guess = readLine("Your guess: ");
        	}
        	result = newResult(result, guess);
        	/* (1)If the result is equal to the word looking for, the loop ends and
        	 * will be displayed a winning message. (2)If the result it`s equal to temp
        	 * it means the user failed to guess a correct letter(new result is the same
        	 * as the old result) and has one less guess. (3)If none of the above it means 
        	 * the user guess one valid letter but not the entire word and the temp
        	 * value will be updated with the current result.
        	 */
        	if ( result.equals(newWord)) {
        		println ("You guessed the word: " + newWord);
        		println ("You win!");
        		canvas.displayWord(newWord);
        		playAnotherGame = readLine ("Do you want to play again? ");
        		break;
        	} else if (result.equals(temp)) {
        		guessesLeft--;
        		println ("There are no " + guess + "`s in the word.");
        		canvas.noteIncorrectGuess(guess.charAt(0));
        	} else  {
        		println("That guess is correct!");
        		temp = result;
        		canvas.displayWord(result);
        	}
    	}
    	/* If the number of guesses is 0 and the result is not equal to the search word
    	 * it means the user has lost the game.
    	 */
    	if (!result.equals(newWord)) {
    		println ("You`re completely hung.");
    		println ("The word was: " + newWord);
    		println ("You lose.");
    		playAnotherGame = readLine ("Do you want to play again? ");
    	}
    }
    /** This method returns a random word from Hangman lexicon class */
    private String secretWord() {
    	/* Generates a random number corresponding to a word in the lexicon list */ 
    	int n = rgen.nextInt(0, (lexicon.getWordCount()-1));  //counting starts from 0
    	return lexicon.getWord(n);
    }
    
    /** This method returns a String of a number of dashed lines equals to 
     *  numbers of characters of word to guess */
    private String dashedLine(String str) {
    	String result = "";
    	for (int i = 0; i < str.length(); i++){
    		result += "-";
    	}
    	return result;
    }
    
    /** This method returns true if the string(character) passed
     *  is a letter and has one character */
    private boolean isValid(String str) {
    	return (Character.isLetter(str.charAt(0)) && str.length() == 1);
    }
    
    /** This method checks if string(character) s is contained in 
     * the word to be guessed and if it is, the character is 
     * entered in the string str(dashed line) in it`s position */
    private String newResult(String str, String s) {
    	for (int j = 0; j < newWord.length(); j++) {
    		if (newWord.charAt(j) == Character.toUpperCase(s.charAt(0))) {
    			str = str.substring(0, j) + newWord.charAt(j)+ str.substring(j+1);
    		}
    	}
    	return str;
    }
    
    /* Private instance variable */
    private HangmanCanvas canvas; // canvas for graphical part
    private HangmanLexicon lexicon;
    private String newWord;  //word to guess
    private int guessesLeft; //keeps track of guesses left
    private RandomGenerator rgen = RandomGenerator.getInstance();
    private String playAnotherGame = "Yes"; //keeps track of the string depending on which starts a new game 
}
