/*
 * File: HangmanLexicon.java
 * -------------------------
 * This file contains a stub implementation of the HangmanLexicon
 * class that you will reimplement for Part III of the assignment.
 */

import acm.util.*;

public class HangmanLexicon {

/** Returns the number of words in the lexicon. */
	public int getWordCount() {
		return 10;
	}

/** Returns the word at the specified index. */
	public String getWord(int index) {
		switch (index) {
			case 0: return "BIBLIOGRAFIE";
			case 1: return "DAMIGEANA";
			case 2: return "TEMPERAMENT";
			case 3: return "GHEINUSE";
			case 4: return "SPANZURATOARE";
			case 5: return "NEINTELIGIBIL";
			case 6: return "ELECTROCARDIOGRAMA";
			case 7: return "INDUSTRIALIZAT";
			case 8: return "PNEUMATIC";
			case 9: return "TELEENCICLOPEDIE";
			default: throw new ErrorException("getWord: Illegal index");
		}
	};
}
