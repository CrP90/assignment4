/*
 * File: HangmanCanvas.java
 * ------------------------
 * This file keeps track of the Hangman display.
 */

import acm.graphics.*;

public class HangmanCanvas extends GCanvas {

/** Resets the display so that only the scaffold appears */
	public void reset() {
		removeAll(); //removes all the object on the canvas
		addScafold();  //adding only scafold
		/* Reset variables wrongGuesses to value one and result to an empty string
		 * so they can keep track of number of wrong guesses in order to display gradually
		 * the body parts, respectively display and update a new list of characters wrong guessed.
		 */
		wrongGuesses = 1;
		result = "";
	}

/**
 * Updates the word on the screen to correspond to the current
 * state of the game.  The argument string shows what letters have
 * been guessed so far; unguessed letters are indicated by hyphens.
 */
	public void displayWord(String word) {
		/* After each guess the label must be updated so after each call of this 
		 * method if the variable which stores the label is not empty will be removed
		 * and then assigned a new updated label.
		 */
		if (wordToGuess != null) remove(wordToGuess);
		wordToGuess = new GLabel(word);
		wordToGuess.setFont("Comic Sans MS-22");
		add(wordToGuess, getWidth() / 10, getHeight()*0.8+wordToGuess.getAscent());
	}

/**
 * Updates the display to correspond to an incorrect guess by the
 * user.  Calling this method causes the next body part to appear
 * on the scaffold and adds the letter to the list of incorrect
 * guesses that appears at the bottom of the window.
 */
	public void noteIncorrectGuess(char letter) {
		/* Like before this method will check the variable that stores the label
		 * after each call doing the same thing. The String variable result initially
		 * defined as an empty string will be update after each call of the method
		 * with the character that represent a wrong guess.
		 */
		if (incorrectGuess != null) remove(incorrectGuess);
		result += letter + " ";
		incorrectGuess = new GLabel(result);
		incorrectGuess.setFont("Tahoma-20");
		add(incorrectGuess, getWidth() / 10, getHeight()*0.87+incorrectGuess.getAscent());
		/* After each call of this method which means a wrong guess a body part has to be
		 * added to the canvas. Every time the method is called the variable wrongGuesses
		 * that keeps track of the numbers of wrong guesses will increase by one and will be
		 * passed to the method addBodyPart who will add a body part depending on wrongGuesses.
		 */
		addBodyPart(wrongGuesses);
		wrongGuesses++;
	}
	/** This method displays the scafold */
	private void addScafold() {
		/* Coordinates for upper part of scafold */
		double x = getWidth()/2 - BEAM_LENGTH;
		double y = ((getHeight()*0.8) - SCAFFOLD_HEIGHT)/2 ;
		GLine scafold = new GLine(x, y, x, y + SCAFFOLD_HEIGHT);
		GLine beam = new GLine (x, y, x+BEAM_LENGTH, y);
		GLine rope = new GLine (x+BEAM_LENGTH, y, x+BEAM_LENGTH, y+ROPE_LENGTH);
		add(scafold);
		add(beam);
		add(rope);
	}
	/** This method will add a body part depending on the parameter passed.
	 * 	First will be added the head, then the body, then each arm, each leg, 
	 *  and finally  each foot�is added to the scaffold until the hanging is complete. 
	 * @param n parameter passed
	 */
	private void addBodyPart(int n) {
		/* Coordinates for upper part of the body */
		double x = getWidth()/2;
		double y = ((getHeight()*0.8) - SCAFFOLD_HEIGHT)/2 + ROPE_LENGTH + 2*HEAD_RADIUS;
		if (n == 1){
			GOval head = new GOval(x - HEAD_RADIUS, y - 2*HEAD_RADIUS, 2*HEAD_RADIUS, 2*HEAD_RADIUS);
			add(head);
		}
		if (n == 2){
			GLine body = new GLine (x, y, x, y+BODY_LENGTH);
			add(body);
		}
		if (n == 3){
			GLine leftUpperArm = new GLine (x, y + ARM_OFFSET_FROM_HEAD, x -
					UPPER_ARM_LENGTH, y + ARM_OFFSET_FROM_HEAD);
			GLine leftLowerArm = new GLine (x - UPPER_ARM_LENGTH, y + ARM_OFFSET_FROM_HEAD, x - 
					UPPER_ARM_LENGTH, y + ARM_OFFSET_FROM_HEAD + LOWER_ARM_LENGTH);
			add(leftUpperArm);
			add(leftLowerArm);
		}
		if (n == 4) {
			GLine rightUpperArm = new GLine (x, y + ARM_OFFSET_FROM_HEAD, x +
					UPPER_ARM_LENGTH, y + ARM_OFFSET_FROM_HEAD);
			GLine rightLowerArm = new GLine (x + UPPER_ARM_LENGTH, y + ARM_OFFSET_FROM_HEAD, x + 
					UPPER_ARM_LENGTH, y + ARM_OFFSET_FROM_HEAD + LOWER_ARM_LENGTH);
			add(rightUpperArm);
			add(rightLowerArm);
		}
		if (n == 5){
			GLine leftHip = new GLine (x, y + BODY_LENGTH, x - HIP_WIDTH, y + BODY_LENGTH);
			GLine leftLeg = new GLine (x - HIP_WIDTH, y + BODY_LENGTH, x - 
					HIP_WIDTH, y + BODY_LENGTH + LEG_LENGTH);
			add(leftHip);
			add(leftLeg);
		}
		if (n == 6){
			GLine rightHip = new GLine (x, y + BODY_LENGTH, x + HIP_WIDTH, y + BODY_LENGTH);
			GLine rightLeg = new GLine (x + HIP_WIDTH, y + BODY_LENGTH, x + 
					HIP_WIDTH, y + BODY_LENGTH + LEG_LENGTH);
			add(rightHip);
			add(rightLeg);
		}
		if (n == 7){
			GLine leftFoot = new GLine (x - HIP_WIDTH, y + BODY_LENGTH + 
					LEG_LENGTH, x - HIP_WIDTH - FOOT_LENGTH, y + BODY_LENGTH + LEG_LENGTH);
			add(leftFoot);	
		}
		if (n == 8) {
			GLine rightFoot = new GLine (x + HIP_WIDTH, y + BODY_LENGTH + 
					LEG_LENGTH, x + HIP_WIDTH + FOOT_LENGTH, y + BODY_LENGTH + LEG_LENGTH);
			add(rightFoot);
		}
	}

/* Constants for the simple version of the picture (in pixels) */
	private static final int SCAFFOLD_HEIGHT = 310;
	private static final int BEAM_LENGTH = 130;
	private static final int ROPE_LENGTH = 12;
	private static final int HEAD_RADIUS = 34;
	private static final int BODY_LENGTH = 130;
	private static final int ARM_OFFSET_FROM_HEAD = 25;
	private static final int UPPER_ARM_LENGTH = 65;
	private static final int LOWER_ARM_LENGTH = 35;
	private static final int HIP_WIDTH = 35;
	private static final int LEG_LENGTH = 100;
	private static final int FOOT_LENGTH = 25;

/* Private instance variable */
	private GLabel wordToGuess;  //label that store and update the word to guess
	private GLabel incorrectGuess; //label that store and update the wrong character guessed
	private String result;  //keeps track of wrong guesses
	private int wrongGuesses;	// keeps track the number of wrong guesses
}
